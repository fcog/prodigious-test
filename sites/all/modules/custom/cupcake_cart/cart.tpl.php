<?php if (!empty($data['cart_items'] )): ?>
	<div class="cart container">
		<div class="row items">
			<?php foreach ($data['cart_items'] as $cart_item): ?>
				<span class="item">
					<?php print '<img title="'.$cart_item->title.'" src="'.image_style_url('thumbnail', $cart_item->field_images['und'][0]['uri']).'" />'?>
				</span>
			<?php endforeach ?>
		</div>
		<div class="row total">
			<span>TOTAL: <?php print $data['total'] ?></span>
			<span>
				<?php
		        $form = drupal_get_form('cupcake_cart_checkout_form');
		        print drupal_render($form);
		        ?>
			</span>
		</div>	
	</div>
<?php endif ?>